package com.accenture.demo.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.accenture.demo.model.Invoice;
import com.accenture.demo.model.PDFTemplate;
import com.accenture.demo.model.ShoppingCart;

/*
 * Servicio y lógica de negocio para las facturas que se creen a partir de los productos que el usuario agregue al carro de compras
 */
@Service
public class InvoiceService {

	private List<Invoice> invoices = new ArrayList<Invoice>();
	private Double homeValue = 18000.0;

	public void saveInvoice(Integer id, ShoppingCart shoppingCart, Double totalValue) {

		if (!findInvoiceById(id, shoppingCart, totalValue, homeValue(totalValue))) {
			Invoice invoice = new Invoice(invoices.size() + 1, totalValue, homeValue(totalValue), shoppingCart);
			invoices.add(invoice);
		}
	}

	/*
	 * Método que valida el costo de los productos con vase en la condición se
	 * determina si paga o no costo de envío.
	 */
	public Double homeValue(Double totalValue) {
		if (totalValue > 100000.0 || totalValue < 7000.0) {
			return homeValue = 0.0;
		}
		return homeValue;
	}

	public ResponseEntity<Object> cratePdf(Integer invoceId, Integer carId) throws IOException {
		Invoice dataPdf = finInvoiseAndCar(invoceId, carId);
		if (dataPdf != null) {
			PDFTemplate pdfTemplate = new PDFTemplate();
			pdfTemplate.build(dataPdf);
		}
		return ResponseEntity.noContent().build();
	}

	public Invoice finInvoiseAndCar(Integer invoceId, Integer carId) {
		for (Invoice dataDb : invoices) {
			if (dataDb.getId().equals(invoceId) && dataDb.getShoppingCart().getId().equals(carId)) {
				return dataDb;
			} else {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Datos no encontrados");
			}
		}
		return null;
	}

	public List<Invoice> getInvoices() {
		return invoices;
	}

	public boolean findInvoiceById(Integer id, ShoppingCart shoppingCart, Double totalValue, Double homeValue) {
		for (Invoice invoiceDb : invoices) {
			if (invoiceDb.getId().equals(id)) {
				invoiceDb.setTotalValue(totalValue);
				invoiceDb.setShoppingCart(shoppingCart);
				invoiceDb.setHomeValue(homeValue);
				return true;
			}
		}
		return false;
	}
}
