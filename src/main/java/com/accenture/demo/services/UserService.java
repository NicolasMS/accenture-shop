package com.accenture.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.accenture.demo.model.User;

/*
 * Servicio que provee la lógica de negocios para la creación de usuarios, búsqueda de los mismos, 
 * validación en el sistema de haber datos similares
 */
@Service
public class UserService {
	private List<User> users = new ArrayList<User>();

	public User save(User user) {
		user.setId(users.size() + 1);
		for (User userDb : users) {
			if (user.getDocumentNumber().equals(userDb.getDocumentNumber())) {
				throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Error al crear el usuario", ""));
			}
		}
		users.add(user);
		return user;
	}

	public List<User> getUsers() {
		return users;
	}

	public User getUsersById(Integer userId) {
		for (User userDb : users) {
			if (userDb.getId().equals(userId)) {
				return userDb;
			}
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no Encontrado");
	}

	/*
	 * Mpetodo que sisrve para validar las credenciales de los usuarios, que no se
	 * presente inconvenientes por el uso de mayúsculas o minúsculas
	 */
	public User getUser(String userName) {
		for (User userDb : users) {
			if (userDb.getName().equalsIgnoreCase(userName)) {
				return userDb;
			}
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario no Encontrado");
	}
}
