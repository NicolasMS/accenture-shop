package com.accenture.demo.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.accenture.demo.model.DateModel;
import com.accenture.demo.model.Product;
import com.accenture.demo.model.ShoppingCart;
import com.accenture.demo.model.User;

/*
 * Servicio de carro de compras define la lófica de negocios, hace uso de métodos para buscar usuarios que tengan productos registrados
 */
@Service
public class ShoppingCartService {

	@Autowired
	private ProductService productService;

	@Autowired
	private UserService userService;

	@Autowired
	private InvoiceService invoiceService;

	private ArrayList<ShoppingCart> shCarts = new ArrayList<ShoppingCart>();

	private List<Product> products = new ArrayList<Product>();

	/*
	 * Método que sirve para añadir un producto al carro, valida que ya haya un
	 * registro en el sistema, valida si las fechas le permite añadir nuevos
	 * productos, y actualiza el registro que se encuentre almacenado, retorna el
	 * nuevo registro
	 */
	public ShoppingCart addCar(Integer idUser, Integer idProduct) throws ParseException {
		DateModel dateModel = new DateModel();
		User user = getUserById(idUser);
		ShoppingCart shCart = searchUserCart(user);
		boolean isModify = dateModel.modifyData();
		if (shCart != null && isModify) {
			products = shCart.getProducts();
			products.add(getProductById(idProduct));
		} else {
			products.add(getProductById(idProduct));
			shCart = new ShoppingCart(shCarts.size() + 1, dateModel.getDateInitial(), dateModel.getDateInitial(),
					getUserById(idUser), products);
		}
		shCarts.add(shCart);
		return shCart;
	}

	public ShoppingCart updateCar(Integer userId, Integer shopId, ShoppingCart shoppingCart) throws ParseException {

		if (getBuy(shopId) != null && getUserById(userId) != null) {

			DateModel dateModel = new DateModel();
			shoppingCart.setModificationDate(dateModel.getDateInitial());
			boolean isModify = dateModel.modifyData();
			if (!isModify) {
				ShoppingCart cartDb = getBuy(shopId);
				Double valueDb = calculateValue(cartDb);
				Double valueNew = calculateValue(shoppingCart);
				products = shoppingCart.getProducts();
				cartDb.setProducts(products);
				if (valueIsMore(valueDb, valueNew)) {
					return shoppingCart;
				} else {
					throw new ResponseStatusException(HttpStatus.CONFLICT, "Error de Sistema");
				}
			} else {
				throw new ResponseStatusException(HttpStatus.CONFLICT, "Límite Excedico");
			}
		}
		return shoppingCart;
	}

	/*
	 * Método que sirve para elmininar un registro del sistema, consulta la
	 * existencia del mismo, valida si puede realizar la acción
	 */
	public ResponseEntity<Object> deleteCar(Integer idCar) throws ParseException {

		DateModel dateModel = new DateModel();
		dateModel.validateDatesInvoice(dateModel.getDateInitial());
		ShoppingCart carDb;
		boolean isModify = dateModel.modifyInvoice();
		if (isModify) {
			carDb = getBuy(idCar);
			if (carDb != null) {
				shCarts.remove(carDb);
			}
		} else {
			carDb = getBuy(idCar);
			Double newValueTotal = calculateValue(carDb) * 0.10;
			invoiceService.saveInvoice(idCar, carDb, newValueTotal);
			throw new ResponseStatusException(HttpStatus.CONFLICT, "Límite Excedico");
		}
		return ResponseEntity.noContent().build();
	}

	/*
	 * Método que sirve para guardar la factura, hace llamado al servicio de las
	 * facturas, y las almacena en el sistema, primero valida si existe el registro
	 * con el identidicador del carrito, calcula los datos y los envía al servicio
	 * de factura
	 */
	public ResponseEntity<Object> saveInvoice(Integer carId) {
		ShoppingCart carDb = getBuy(carId);
		Double totalValue = calculateValue(carDb);
		invoiceService.saveInvoice(carId, carDb, totalValue);
		return ResponseEntity.noContent().build();
	}

	public Double calculateValue(ShoppingCart cart) {
		Double valueProducts = 0.0;
		for (Product data : cart.getProducts()) {
			valueProducts += data.getValue();
		}
		return valueProducts;
	}

	/*
	 * Método que valida si el valor de la base de datos es menor o igual el el
	 * nuevo valor que se genera en el nuevo registro
	 */
	public boolean valueIsMore(Double valueDb, Double newValue) {
		if (valueDb <= newValue) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Método que sirve para buscar el usuario en un registro de compra
	 */
	public ShoppingCart searchUserCart(User user) {
		for (ShoppingCart cartDb : shCarts) {
			if (cartDb.getUser().getDocumentNumber().equals(user.getDocumentNumber())) {
				return cartDb;
			} else {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Error");
			}
		}
		return null;
	}

	/*
	 * Método que sirve para retornar la compra de acuerdo al identificador
	 * proporcionado
	 */
	public ShoppingCart getBuy(Integer buyId) {
		for (ShoppingCart buy : shCarts) {
			if (buy.getId().equals(buyId)) {
				return buy;
			} else {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Compra no Encontra");
			}
		}
		return null;
	}

	public List<ShoppingCart> getShoppingCarts() {
		return shCarts;
	}

	public User getUserById(Integer userId) {
		return userService.getUsersById(userId);
	}

	public Product getProductById(Integer productId) {
		return productService.getProductById(productId);
	}
}
