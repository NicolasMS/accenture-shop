package com.accenture.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.accenture.demo.model.Product;
/*
 * Servicio que incluye lógica de negocios para los productos.
 */

@Service
public class ProductService {
	private List<Product> products = new ArrayList<Product>();

	public void save(Product product) {
		products.add(product);
	}

	public List<Product> getProducts() {
		return products;
	}

	public Product getProductById(Integer productId) {
		for (Product productDb : products) {
			if (productDb.getId().equals(productId)) {
				return productDb;
			}
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Producto no encontrado ", ""));
	}
}
