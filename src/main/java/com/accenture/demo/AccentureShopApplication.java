package com.accenture.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.accenture.demo.model.Product;
import com.accenture.demo.services.ProductService;
import com.github.javafaker.Faker;

@SpringBootApplication
public class AccentureShopApplication implements ApplicationRunner {

	@Autowired
	private Faker faker;

	@Autowired
	private ProductService productRepositories;

	public static void main(String[] args) {
		SpringApplication.run(AccentureShopApplication.class, args);
	}

	/*
	 * Se sobre escribe el método para se permita cargar la información de los
	 * productos al iniciar el sistema
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {

		for (int i = 0; i < 10; i++) {
			productRepositories.save(new Product(i + 1, faker.food().vegetable(), (i + 1) * 1000.0));
		}
	}

}
