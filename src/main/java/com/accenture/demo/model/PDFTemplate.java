package com.accenture.demo.model;

import java.awt.Color;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

public class PDFTemplate {
	private static final String tittle = "Accenture Shop";

	public PDFTemplate() {

	}

	public void build(Invoice invoice) throws IOException {
		PDDocument document = new PDDocument();
		PDPage page = new PDPage(PDRectangle.A4);
		document.addPage(page);

		PDPageContentStream contentStream = new PDPageContentStream(document, page);

		// Título
		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(192, 65, 188));
		contentStream.setFont(PDType1Font.TIMES_BOLD, 40);
		contentStream.newLineAtOffset(20, page.getMediaBox().getHeight() - 52);
		contentStream.showText(tittle);
		contentStream.endText();

		// Imagen
		PDImageXObject image = PDImageXObject.createFromByteArray(document,
				PDFTemplate.class.getResourceAsStream("../assets/image.jpg").readAllBytes(), "Accenture Shop");
		contentStream.drawImage(image, 450, page.getMediaBox().getHeight() - 150, image.getWidth() / 4,
				image.getHeight() / 4);


		// Título productos
		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(167, 93, 167));
		contentStream.setFont(PDType1Font.HELVETICA, 15);
		contentStream.newLineAtOffset(50, page.getMediaBox().getHeight() - (170));
		contentStream.showText("Productos");
		contentStream.endText();

		int y = 250;
		int index = 0;
		for (Product data : invoice.getShoppingCart().getProducts()) {
			contentStream.beginText();
			contentStream.setNonStrokingColor(new Color(0, 0, 0));
			contentStream.setFont(PDType1Font.HELVETICA, 10);
			contentStream.newLineAtOffset(50, page.getMediaBox().getHeight() - (y));
			contentStream.showText((index + 1) + ". " + data.getName());
			contentStream.endText();
			index++;
			y += 50;

		}

		// Titulo datos ccliente y factura

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(167, 93, 167));
		contentStream.setFont(PDType1Font.HELVETICA, 15);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 170);
		contentStream.showText("Datos Cliente");
		contentStream.endText();

		// Datos cliente y Factura 
		
		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 250);
		contentStream.showText("Nombre: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 250);
		contentStream.showText(invoice.getShoppingCart().getUser().getName());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 300);
		contentStream.showText("N.Documento: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 300);
		contentStream.showText(invoice.getShoppingCart().getUser().getDocumentNumber());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 350);
		contentStream.showText("Dirección: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 350);
		contentStream.showText(invoice.getShoppingCart().getUser().getAddres());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 400);
		contentStream.showText("Total a pagar: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 400);
		contentStream.showText(invoice.getTotalValue().toString());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 450);
		contentStream.showText("IVA: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 450);
		contentStream.showText(invoice.getIva().toString());
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(0, 0, 0));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(300, page.getMediaBox().getHeight() - 500);
		contentStream.showText("Valor Domicilio: ");
		contentStream.endText();

		contentStream.beginText();
		contentStream.setNonStrokingColor(new Color(113, 107, 113));
		contentStream.setFont(PDType1Font.HELVETICA, 10);
		contentStream.newLineAtOffset(460, page.getMediaBox().getHeight() - 500);
		contentStream.showText(invoice.getHomeValue().toString());
		contentStream.endText();
		
		// Cerrar Flujo
		contentStream.close();

		
		// Guardar
		document.save("document.pdf");
	}
}
