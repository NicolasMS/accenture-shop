package com.accenture.demo.model;

public class User {
	private Integer id;
	private String name;
	private String addres;
	private String documentNumber;

	public User() {

	}

	public User(Integer id, String name, String addres, String documentNumber) {
		this.id = id;
		this.name = name;
		this.addres = addres;
		this.documentNumber = documentNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddres() {
		return addres;
	}

	public void setAddres(String addres) {
		this.addres = addres;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

}
