package com.accenture.demo.model;

public class Invoice {
	private Integer id;
	private Double totalValue;
	private ShoppingCart shoppingCart;

	private final Double iva = 0.19;
	private Double homeValue = 10.0;

	public Invoice() {

	}

	public Invoice(Integer id, Double totalValue, Double homeValue, ShoppingCart shoppingCart) {
		this.id = id;
		this.totalValue = totalValue;
		this.homeValue = homeValue;
		this.shoppingCart = shoppingCart;

	}

	public Integer getId() {
		return id;
	}

	public Double getIva() {
		return iva * totalValue;
	}

	public Double getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public void setHomeValue(Double homeValue) {
		this.homeValue = homeValue;
	}

	public Double getHomeValue() {
		return homeValue;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	@Override
	public String toString() {
		return "Invoice [id=" + id + ", totalValue=" + totalValue + ", shoppingCart=" + shoppingCart + ", iva=" + iva
				+ ", homeValue=" + homeValue + "]";
	}

}
