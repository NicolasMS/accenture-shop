package com.accenture.demo.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/*
 * Clase que permite generar las fechas que van a ser asignasdas cuando el usuario creen Carrito de compra
 * de igual manera sirve para validar el rango de las fechas y determinar si puede elminiar el pedido, o
 * acualizar el mismo.  
 */

public class DateModel {
	private final String format = "HH: mm: ss";
	private final String limit = "05: 00: 00";
	private final String limitInvoice = "12: 00: 00";
	private Date dateInitial;
	private Date dateFinally;
	private boolean isModify;
	private boolean isModifyInvoice;
	private SimpleDateFormat simpleDateFormat;

	public DateModel() throws ParseException {
		dateInitial = new Date();
		simpleDateFormat = new SimpleDateFormat(format, Locale.UK);
		dateInitial = simpleDateFormat.parse(simpleDateFormat.format(dateInitial));
		dateFinally = simpleDateFormat.parse(limit);
	}

	/*
	 * Método que valida las fechas para las facturas con rango de 12 horas para
	 * determinar si puede eliminar el producto
	 */
	public void validateDatesInvoice(String initial) throws ParseException {

		simpleDateFormat = new SimpleDateFormat(format);
		dateInitial = simpleDateFormat.parse(initial);
		dateFinally = simpleDateFormat.parse(limitInvoice);
		Date diference = diference(dateInitial, dateFinally);

		if (diference.before(dateFinally)) {
			isModifyInvoice = false;
		} else {
			isModifyInvoice = true;
		}
	}

	/*
	 * Método que valida si se puede editar el carrito de compras
	 */
	public void validateDates(String initial) throws ParseException {
		simpleDateFormat = new SimpleDateFormat(format);
		dateInitial = simpleDateFormat.parse(initial);
		dateFinally = simpleDateFormat.parse(limit);
		Date diference = diference(dateInitial, dateFinally);
		if (diference.before(dateFinally)) {
			isModify = true;
		} else {
			isModify = false;
		}
	}

	public boolean modifyData() {
		return isModify;
	}

	public boolean modifyInvoice() {
		return isModifyInvoice;
	}

	/*
	 * Método que valida la diferencia de fechas con respecto a las que se envian
	 * por parámetro que son fechas de modificación y creación de productos. Todo en
	 * base a la fecha actual que el sistema ofrece.
	 */
	public Date diference(Date dateInitial, Date dateFinally) {
		long milliseconds = dateFinally.getTime() - dateInitial.getTime();
		int seconds = (int) (milliseconds / 1000) % 60;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.SECOND, seconds);
		c.set(Calendar.MINUTE, minutes);
		c.set(Calendar.HOUR_OF_DAY, hours);
		return c.getTime();
	}

	public String getDateInitial() {
		simpleDateFormat = new SimpleDateFormat(format, Locale.UK);
		return simpleDateFormat.format(dateInitial);
	}

	public void setDateInitial(Date dateInitial) {
		this.dateInitial = dateInitial;
	}

	public String getDateFinally() {
		simpleDateFormat = new SimpleDateFormat(format, Locale.UK);
		return simpleDateFormat.format(dateFinally);
	}

	public void setDateFinally(Date dateFinally) {
		this.dateFinally = dateFinally;
	}

}
