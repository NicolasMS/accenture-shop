package com.accenture.demo.model;

import java.util.List;

public class ShoppingCart {
	private Integer id;
	private String creationDate;
	private String modificationDate;
	private User user;
	private List<Product> products;

	public ShoppingCart() {

	}

	public ShoppingCart(Integer id, String creationDate, String modificationDate, User user, List<Product> products) {
		this.id = id;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.user = user;
		this.products = products;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(String modificationDate) {
		this.modificationDate = modificationDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	public void addProduct(Product product) {
		this.products.add(product);
	}

	@Override
	public String toString() {
		return "ShoppingCart [id=" + id + ", creationDate=" + creationDate + ", modificationDate=" + modificationDate
				+ ", user=" + user + ", products=" + products + "]";
	}

}
