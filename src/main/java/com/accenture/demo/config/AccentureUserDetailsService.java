package com.accenture.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.accenture.demo.model.User;
import com.accenture.demo.services.UserService;

/*
 * Servicio que sirve para determinar la existencia del usuario.
 */
@Service
public class AccentureUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User userDb = userService.getUser(username);
		if (userDb != null) {
			return org.springframework.security.core.userdetails.User.withUsername("Pedro")
					.password(passwordEncoder.encode("123")).roles("").build();
		} else {
			throw new UsernameNotFoundException("Username " + username + " no encontrado");
		}
	}

}
