package com.accenture.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.javafaker.Faker;

/*
 * Servicio externo que proporciona los nombres de los productos
 */
@Configuration
public class FakerBeanConfig {
	@Bean
	public Faker getFaker() {
		return new Faker();
	}
}