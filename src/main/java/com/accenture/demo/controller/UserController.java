package com.accenture.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.model.User;
import com.accenture.demo.services.UserService;

/*
 * Controlador de los endpoints de los usuarios.
 *  MediaType.APPLICATION_FORM_URLENCODED_VALUE: permite que los datos lleguen en formato x-www-form-urlencoded
 */
@RestController
@RequestMapping(path = "/usuario")
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping(consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public ResponseEntity<User> saveUser(User user) {
		return new ResponseEntity<User>(userService.save(user), HttpStatus.CREATED);
	}
}
