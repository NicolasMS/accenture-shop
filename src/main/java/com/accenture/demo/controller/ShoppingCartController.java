package com.accenture.demo.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.model.ShoppingCart;
import com.accenture.demo.services.ShoppingCartService;

/*
 * Controlador de los endpoints de los carros de compras.
 */
@RestController
@RequestMapping(path = "/compra")
public class ShoppingCartController {
	@Autowired
	private ShoppingCartService shoppinCartS;

	@GetMapping(value = "/producto/{productId}/usuario/{userId}")
	public ResponseEntity<ShoppingCart> addCar(@PathVariable("productId") Integer productId,
			@PathVariable("userId") Integer userId) throws ParseException {
		return new ResponseEntity<ShoppingCart>(shoppinCartS.addCar(userId, productId), HttpStatus.CREATED);
	}

	@GetMapping()
	public ResponseEntity<List<ShoppingCart>> getCar() {
		return new ResponseEntity<List<ShoppingCart>>(shoppinCartS.getShoppingCarts(), HttpStatus.OK);
	}

	@PutMapping("/{byusId}/usuario/{userId}")
	public ResponseEntity<ShoppingCart> updateCar(@PathVariable(name = "byusId") Integer byuId,
			@PathVariable(name = "userId") Integer userId, @RequestBody ShoppingCart shoppingCart)
			throws ParseException {
		return new ResponseEntity<ShoppingCart>(shoppinCartS.updateCar(userId, byuId, shoppingCart), HttpStatus.OK);
	}

	@GetMapping("/factura/{carId}")
	public ResponseEntity<Object> generateInvoice(@PathVariable(name = "carId") Integer carId) {
		return new ResponseEntity<Object>(shoppinCartS.saveInvoice(carId), HttpStatus.CREATED);
	}

	@DeleteMapping("/{idCar}")
	public ResponseEntity<Object> deleteCar(@PathVariable(name = "idCar") Integer idCar) throws ParseException {
		return new ResponseEntity<Object>(shoppinCartS.deleteCar(idCar), HttpStatus.OK);
	}
}
