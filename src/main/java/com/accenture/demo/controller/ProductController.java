package com.accenture.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.model.Product;
import com.accenture.demo.services.ProductService;

/*
 *Controlador de los endpoints de productos 
 */

@RestController
@RequestMapping(path = "/producto")
public class ProductController {
	@Autowired
	private ProductService productService;

	@GetMapping(path = "productos")
	public ResponseEntity<List<Product>> getProducts() {
		return new ResponseEntity<List<Product>>(productService.getProducts(), HttpStatus.OK);
	}

}
