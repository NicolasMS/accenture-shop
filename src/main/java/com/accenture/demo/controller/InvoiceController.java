package com.accenture.demo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.demo.model.Invoice;
import com.accenture.demo.services.InvoiceService;

/*
 * Controlador de endpoints para las facturas
 */

@RestController
@RequestMapping(path = "/factura")
public class InvoiceController {

	@Autowired
	private InvoiceService invoiceService;

	@GetMapping()
	public ResponseEntity<List<Invoice>> getInvoices() {
		return new ResponseEntity<List<Invoice>>(invoiceService.getInvoices(), HttpStatus.OK);
	}

	@GetMapping("/generar/user/{userId}/compra/{compraId}")
	public ResponseEntity<Object> createPdf(@PathVariable(name = "userId") Integer userId,
			@PathVariable(name = "compraId") Integer carId) throws IOException {
		return new ResponseEntity<Object>(invoiceService.cratePdf(userId, carId), HttpStatus.OK);
	}
}
