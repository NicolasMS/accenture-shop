Accenture-Shop
////////////////////////////////////////////////////////////////////////////////////////////////
Consideraciones a tener en cuenta:
-	Usuario y número de documento es obligatorio para hacer uso de los End-Points.
-	Rutas protegidas con “Basic-Autentication”.
-	No se admite usuarios con el mismo número de documento de identidad.
-	La factura es Generada en la raíz de proyecto.
-	Colección de rutas añadidas en el archivo “Accenture-Shop.postman_collection.json”.
-	Usar Postman, para probar los End-Points.
